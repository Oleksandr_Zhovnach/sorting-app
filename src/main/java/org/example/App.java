package org.example;

public class App {
    public int[] sort(int[] numbers) {
        if (numbers == null) {
            throw new IllegalArgumentException("Array mast not be null");
        }
        if (numbers.length>10) {
            throw new IllegalArgumentException("Array must not have more 10 elements");
        }
        int length = numbers.length;
        if (numbers.length<=1) {
            return numbers;
        }
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length - 1; j++) {
                if (numbers[j] > numbers[j + 1]) {
                    int temp = numbers[j + 1];
                    numbers[j + 1] = numbers[j];
                    numbers[j] = temp;
                }
            }
        }
        return numbers;
    }
}

