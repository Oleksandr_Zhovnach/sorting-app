package org.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int [] numbers = new int[10];
        Scanner scanner = new Scanner(System.in);
        for (int i =0;i<10;i++) {
            numbers[i] = scanner.nextInt();
        }
        App app = new App();
        app.sort(numbers);
        for (int item:numbers) {
            System.out.println(item);
        }
    }
}
