package org.example.test;

import org.example.App;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;
import static org.junit.Assert.assertArrayEquals;



@RunWith(Parameterized.class)
public class AppTest {
    int [] params;
    int [] result;
    App app = new App();
    public AppTest(int[] params,int [] result) {
      this.params = params;
      this.result = result;
    }
    @Parameters
    public static Collection<Object[]> parameters() {
        return Arrays.asList(new Object[][]{
                {new int[]{888,70,1,6,9,4,46,7,5,99},new int[]{1,4,5,6,7,9,46,70,99,888}},
                {new int[]{1,6,9,4,46,46,46,46,46,7},new int[]{1,4,6,7,9,46,46,46,46,46}},
                {new int[]{10,6,9,4,46,46,46,46,46,7},new int[]{4,6,7,9,10,46,46,46,46,46}},
                {new int[]{1,6,187,4,46,46,33,877,46,7},new int[]{1,4,6,7,33,46,46,46,187,877}},

        });
    }
    @Test
    public void testTenNumbers() {
        assertArrayEquals(app.sort(params),result);
    }
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void testOneNumbers() {
      int [] numbers = new int[]{7};
      int [] result = new int[]{7};
      assertArrayEquals(app.sort(numbers),result);
    }


    @Test
    public void testZeroNumbers() {
        int [] numbers = null;
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Array mast not be null");
        app.sort(numbers);

    }
    @Test
    public void testMoreTenNumbers() {
        int[] numbers = new int[]{8, 54, 4, 7, 23, 98, 679, 0, 6, 0, 97, 8};
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Array must not have more 10 elements");
        app.sort(numbers);
    }


}
